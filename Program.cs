﻿using System;
using System.Text;

namespace TestCore
{
    public class WierdList
    {
        class ListElement
        {
            public ListElement Next;
            public ListElement Prev;

            public int Data;
        }

        private ListElement Head;
        private ListElement Last;

        public void AddLast(int num)
        {
            ListElement elem = new ListElement();
            elem.Data = num;

            if (Head == null)
            {
                Head = elem;
            }
            else
            {
                Last.Next = elem;
                elem.Prev = Last;
            }

            Last = elem;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            ListElement elem = Head;
            
            while(elem != null)
            {
                sb.Append(elem.Data);
                sb.Append(" ");
                elem = elem.Next;
            }

            return sb.ToString();
        }
        public void AddFirst(int num)
        {
            ListElement elem = new ListElement();

            elem.Data = num;

            if (Head != null)
            {
                
                elem.Next = Head;
                Head.Prev = elem;
                Head = elem;
            }
            else
            {
                Head.Next = elem;
            }

        }

        public void Add(int num, int index)
        {
            ListElement elem = new ListElement();
            ListElement iterator = Head;
            elem.Data = num;
            int i = 0;
            while(index != i)
            {
                iterator = iterator.Next;
                i++;
            }
            elem.Next = iterator;
            elem.Prev = iterator.Prev;
            iterator.Prev.Next = elem;
            iterator.Prev = elem;
        }
        public void Remove(int index)
        {
            ListElement iterator = Head;
            int i = 0;
            while (index != i)
            {
                iterator = iterator.Next;
                i++;
            }
            iterator.Next.Prev = iterator.Prev;
            iterator.Prev.Next = iterator.Next;
        }

    }

    public class Program
    {

        public static void Main()
        {
            WierdList list = new WierdList();
            list.AddLast(1);
            list.AddLast(2);
            list.AddLast(3);
            list.AddFirst(5);
            list.Add(4, 1);
            list.Remove(2);

            Console.WriteLine(list.ToString());
        }
    }
}